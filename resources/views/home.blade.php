@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <diary  :tasks="{{ $tasks }}" :contacts="{{ $contacts }}" ></diary>
        </div>
    </div>
</div>
@endsection
