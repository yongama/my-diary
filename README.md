# WarpIT

This hase been built in Laravel 5.7 and Vue.js

Step to set up and run:

1. After cloning, run the following commands

$ cd /path/to/project/directory

2. Set up a database for it and update the .env file and run the following commands:

$ composer install

$ php artisan config:cache

$ php artisan migrate 

$ npm install

$ npm i vue bootstrap-vue bootstrap

$ php artisan serve

For email reminders, the application uses built in laravel task scheduler. You do however need to have the following entry in your cron table.

* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1  #Every minute, for testing.

The scheduling code is locate in /app/Console/Kernel.php , the schedule method.

