<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
   protected $fillable = [
		'user_id',
		'first_name',
		'last_name',
    ];

    public function attributes(){
    	return $this->hasMany('App\ContactAttributes', 'contact_id');
    }
}
