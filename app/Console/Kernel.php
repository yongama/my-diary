<?php

namespace App\Console;

use Auth;
use DB;
use App\User;
use App\Task;
use Carbon\Carbon as Carbon;
use App\Notifications\TaskDue;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // Use this line for testing
        // $schedule->call($this->sendReminder())->everyMinute();

        $schedule->call($this->sendReminder())->dailyAt('08:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Send an email notification about the task due today
     *
     * @return void
     */
    public function sendReminder(){

        $today = Carbon::today();

        $due_tasks =  Task::whereDate('due_date', $today)->where(['complete' => 0])->where(['deleted' => 0])->get();

        foreach ($due_tasks as $key => $task) {
            $user = User::find($task->user_id);
            $user->notify(new TaskDue($user, $task));
        }
    }
}
