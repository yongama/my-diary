<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactAttributes extends Model
{
   	protected $fillable = [
		'contact_id',
		'contact_number',
		'email',
		'address'
    ];
}
