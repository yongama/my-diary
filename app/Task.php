<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
   protected $fillable = [
		'user_id',
		'title',
		'body',
		'priority',
		'due_date',
		'deleted',
		'complete',
    ];
}
