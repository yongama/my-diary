<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Task;
use App\Contact;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/login');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
            $tasks_collection = Task::orderBy('due_date', 'DESC')->orderBy('priority', 'DESC')->where(['user_id' => Auth::user()->id])->where(['complete' => 0])->where(['deleted' => 0])->get();
            $tasks = json_encode($tasks_collection);

            $contacts_collection = Contact::with('attributes')->where(['user_id' => Auth::user()->id])->get();
            $contacts = json_encode($contacts_collection);

            return view('home')->with(compact('contacts','tasks'));
    }
}
