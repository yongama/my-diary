<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Task;
use App\Contact;
use App\ContactAttributes;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $details = $request->details;
        $info_rows = $request->info_rows;
        
        try{
            DB::beginTransaction();

            $contact = Contact::create([
                'user_id' => Auth::user()->id,
                'first_name' => $details['first_name'],
                'last_name' => $details['last_name'],
            ]);

            foreach ($info_rows as $key => $row) {
                ContactAttributes::create([
                    'contact_id' => $contact['id'],
                    'contact_number' => $row['contact_number'] ,
                    'email' => $row['email'] ,
                    'address' => $row['address']
                ]);
            }

            DB::commit();
            return array('success' => true,'message' => 'Contact has been added succesfully');

        }catch(\QueryException $e){
            DB::rollback();
            return array('success' =>false,'message' => $e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $details = $request->details;
        $info_rows = $request->info_rows;

        try{
            DB::beginTransaction();

            Contact::where(['id' => $details['id']])->update([
                'first_name' => $details['first_name'],
                'last_name' => $details['last_name'],
            ]);

            ContactAttributes::where(['contact_id' => $details['id']])->delete();

            foreach ($info_rows as $key => $row) {

                ContactAttributes::create([
                    'contact_id' => $details['id'],
                    'contact_number' => $row['contact_number'] ,
                    'email' => $row['email'] ,
                    'address' => $row['address']
                ]);
            }

            DB::commit();
            return array('success' => true,'message' => 'Contact has been added succesfully');

        }catch(\QueryException $e){
            DB::rollback();
            return array('success' =>false,'message' => $e);
        }       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = null)
    {
        $contact = Contact::findOrFail($id);
        if($contact){
            $contact->delete();
            ContactAttributes::where(['contact_id' => $id])->delete();
            return array('success' =>true);
        }else{
            return array('success' =>false);
        }        
    }

    public function getContacts(){
        $contacts = Contact::with('attributes')->where(['user_id' => Auth::user()->id])->get();

        return ['contacts' => $contacts];
    }
}
