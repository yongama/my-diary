<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Task;
use App\Contact;
use Carbon\Carbon as Carbon;
use App\ContactAttributes;
use Illuminate\Http\Request;
use App\Notifications\TaskDue;

class TaskController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = $request->task;
        
        try{
            DB::beginTransaction();

            $task_create = Task::create([
                'user_id' => Auth::user()->id,
                'title' => $task['title'],
                'body' => $task['body'],
                'priority' => $task['priority'],
                'due_date' => date('Y-m-d H:m:s', strtotime( $task['due_date']) ) ,
            ]);

            DB::commit();
            return array('success' => true,'message' => 'Your task has been added succesfully');

        }catch(\QueryException $e){
            DB::rollback();
            return array('success' =>false,'message' => $e);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Task $task)
    {
        $task = $request->task;
        try{
            DB::beginTransaction();

            Task::where(['id' => $task['id']])->update([
                'title' => $task['title'],
                'body' => $task['body'],
                'priority' => $task['priority'],
                'due_date' => date('Y-m-d', strtotime( $task['due_date']) ) ,
            ]);

            DB::commit();
            return array('success' => true,'message' => 'Your task has been added succesfully');

        }catch(\QueryException $e){
            DB::rollback();
            return array('success' =>false,'message' => $e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update($action, $task_id)
    {
    
        try{
            DB::beginTransaction();

            switch ($action) {
                case 'delete':
                        Task::where(['id' => $task_id])->update([
                            'deleted' => 1,
                        ]);
                    break;

                case 'complete':
                        Task::where(['id' => $task_id])->update([
                            'complete' => 1,
                        ]);
                    break;
            }

            DB::commit();
            return array('success' => true,'message' => 'Your task has been updated succesfully');

        }catch(\QueryException $e){
            DB::rollback();
            return array('success' =>false,'message' => $e);
        }
    }

    public function getTasks(){
        $tasks = Task::orderBy('due_date', 'ASC')->orderBy('priority', 'DESC')->where(['user_id' => Auth::user()->id])->where(['complete' => 0])->where(['deleted' => 0])->get();
        return ['tasks' => $tasks];
    }
}
