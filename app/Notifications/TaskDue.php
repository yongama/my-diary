<?php

namespace App\Notifications;

use App\User;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskDue extends Notification
{
    use Queueable;
    public $task;
    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Task $task)
    {
        $this->task = $task;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $priority = ( $this->task->priority )? 'High' : 'Low';

        return (new MailMessage)
                    ->subject('Task Due')
                    ->greeting('Hello ' . $this->user->name .'!' )
                    ->line('The following task is due today.')
                    ->line('Title: ' . $this->task->title)
                    ->line('Body: ' . $this->task->body)
                    ->line('Priority: ' . $priority)
                    ->action('Manage Tasks', url('/'))
                    ->line('Thank you!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
