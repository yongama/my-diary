<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@home')->name('home');

// Tasks routes
Route::get('/get-all-tasks', 'TaskController@getTasks');
Route::post('/add-task', 'TaskController@store');
Route::post('/edit-task', 'TaskController@edit');
Route::get('/update-task/{action}/{task_id}', 'TaskController@update');

// Contacts routes
Route::get('/get-all-contacts', 'ContactController@getContacts');
Route::get('/delete-contact/{id}', 'ContactController@destroy');
Route::post('/add-contact', 'ContactController@store');
Route::post('/update-contact', 'ContactController@update');


